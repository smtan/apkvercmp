# Intro
This repo is meant to help test the accuracy of adding apk support to [semver_dialects](gitlab.com/gitlab-org/ruby/gems/semver_dialects).
It is a copy of the [apk_version_compare](https://gitlab.alpinelinux.org/alpine/apk-tools/-/blob/master/src/version.c) code from [apk-tools](https://gitlab.alpinelinux.org/alpine/apk-tools) where I hardcoded the dependent methods referenced in other files.

# Build
gcc main.c -o apkvercmp

# Usage
./apkvercmp v1 v2

# Debug
- Install the C/C++ extension in vscode
- In vscode, place breakpoints and run debugger