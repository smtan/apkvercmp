#include "version.c"

const char *get_version_comparison_result(apk_blob_t version_a, apk_blob_t version_b) {
    int result = apk_version_compare(version_a, version_b);
    switch (result) {
        case APK_VERSION_UNKNOWN:
            return "Unknown";
        case APK_VERSION_EQUAL:
            return "Equal";
        case APK_VERSION_LESS:
            return "Less";
        case APK_VERSION_GREATER:
            return "Greater";
        case APK_VERSION_FUZZY:
            return "Fuzzy";
        case APK_VERSION_CONFLICT:
            return "Conflict";
        default:
            return "Unknown";
    }
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("Usage: %s <version1> <version2>\n", argv[0]);
        return 1;
    }

    const char *version_str_a = argv[1];
    const char *version_str_b = argv[2];

    // Convert version strings to apk_blob_t objects
    apk_blob_t version_a = APK_BLOB_STR(version_str_a);
    apk_blob_t version_b = APK_BLOB_STR(version_str_b);

    // Call apk_version_compare function to compare the versions
    const char *result_str = get_version_comparison_result(version_a, version_b);
    printf("%s is %s than %s\n", version_str_a, result_str, version_str_b);
    return 0;
}